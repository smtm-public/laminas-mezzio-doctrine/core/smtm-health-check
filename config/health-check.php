<?php

declare(strict_types=1);

namespace Smtm\HealthCheck;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-health-check')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../',
        '.env.smtm.smtm-health-check'
    );
    $dotenv->load();
}

return [
    'health-check' =>
        json_decode(
            EnvHelper::getEnvFromProcessOrSuperGlobal(['SMTM_HEALTH_CHECK_EXPOSED_ROUTES', 'HEALTH_CHECK_EXPOSED_ROUTES'], '[]'),
            true
        )
];
