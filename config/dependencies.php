<?php

declare(strict_types=1);

namespace Smtm\HealthCheck;

use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Factory\DoctrineConfigAwareDelegator;
use Smtm\HealthCheck\Context\DbConnection\Application\Service\DbConnectionService;
use Psr\Container\ContainerInterface;

return [
    'delegators' => [
        ApplicationServicePluginManager::class => [
            function (
                ContainerInterface $container,
                $name,
                callable $callback,
                array $options = null
            ) {
                /** @var ApplicationServicePluginManager $applicationServicePluginManager */
                $applicationServicePluginManager = $callback();
                $applicationServicePluginManager->addDelegator(
                    DbConnectionService::class,
                    DoctrineConfigAwareDelegator::class
                );

                return $applicationServicePluginManager;
            }
        ],
    ],
];
