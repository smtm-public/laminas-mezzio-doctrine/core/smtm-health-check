<?php

declare(strict_types=1);

namespace Smtm\HealthCheck;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-health-check')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../',
        '.env.smtm.smtm-health-check'
    );
    $dotenv->load();
}

$exposedRoutes = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal(['SMTM_HEALTH_CHECK_EXPOSED_ROUTES', 'HEALTH_CHECK_EXPOSED_ROUTES'], '[]'),
    true
);
$exposedRoutes = array_combine($exposedRoutes, $exposedRoutes);

$routes = [
    // HealthCheck
    'smtm.health-check.db-connection.index' => [
        'path' => '/health-check/db-connection',
        'method' => 'get',
        'middleware' => Context\DbConnection\Http\Handler\IndexHandler::class,
        'options' => [
            'authenticationServiceNames' => null,
            'authorizationServiceNames' => null,
        ],
    ],
];

return array_intersect_key($routes, $exposedRoutes);
