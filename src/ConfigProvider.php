<?php

declare(strict_types=1);

namespace Smtm\HealthCheck;

use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ConfigProvider
{
    #[ArrayShape([
        'dependencies' => 'array',
        'routes' => 'array',
        'health-check' => 'array'
    ])] public function __invoke(): array
    {
        return [
            'dependencies' => include __DIR__ . '/../config/dependencies.php',
            'routes' => include __DIR__ . '/../config/routes.php',
            'health-check' => include __DIR__ . '/../config/health-check.php',
        ];
    }
}
