<?php

declare(strict_types=1);

namespace Smtm\HealthCheck\Context\DbConnection\Application\Service;

use Smtm\Base\Application\Service\AbstractApplicationService;
use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;
use Smtm\HealthCheck\Context\DbConnection\Application\Hydrator\DbConnectiontHydrator;
use Smtm\HealthCheck\Context\DbConnection\Domain\DbConnection;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DbConnectionService extends AbstractApplicationService implements ConfigAwareInterface
{

    use ConfigAwareTrait;

    protected ?string $domainObjectName = DbConnection::class;
    protected ?string $hydratorName = DbConnectiontHydrator::class;
}
