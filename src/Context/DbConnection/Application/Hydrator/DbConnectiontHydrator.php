<?php

declare(strict_types=1);

namespace Smtm\HealthCheck\Context\DbConnection\Application\Hydrator;

use Smtm\Base\Application\Hydrator\DomainObjectHydrator;

class DbConnectiontHydrator extends DomainObjectHydrator
{
    /** @inheritdoc  */
    protected array $mustHydrate = [
        'status' =>
            'You must specify a status for the DbConnection.',
    ];

    protected array $properties = [

    ];
}
