<?php

declare(strict_types=1);

namespace Smtm\HealthCheck\Context\DbConnection\Application\Extractor;

use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DbConnectionExtractor extends AbstractDomainObjectExtractor
{
    protected array $properties = [
        'name' => null,
        'status' => null,
    ];
}
