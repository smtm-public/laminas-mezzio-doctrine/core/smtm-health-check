<?php

declare(strict_types=1);

namespace Smtm\HealthCheck\Context\DbConnection\Http\Handler;

use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;
use Smtm\Base\Http\Handler\DbServiceEntity\UuidAware\AbstractIndexHandler;
use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Smtm\HealthCheck\Context\DbConnection\Application\Extractor\DbConnectionExtractor;
use Smtm\HealthCheck\Context\DbConnection\Application\Service\DbConnectionService;
use Smtm\HealthCheck\Context\DbConnection\Domain\DbConnection;
use Doctrine\ORM\EntityManagerInterface;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Nikolay Mikov <n.mikov@smtm.com>
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class IndexHandler extends AbstractIndexHandler
{
    public ?string $domainObjectExtractorName = DbConnectionExtractor::class;

    public function handle(ServerRequestInterface $request): JsonResponse
    {
        /** @var DbConnectionService $dbConnectionService */
        $dbConnectionService = $this->applicationServicePluginManager->get(DbConnectionService::class);
        $config = $dbConnectionService->getConfig();

        $dbConnectionArray = [];
        $httpStatus = HttpHelper::STATUS_CODE_OK;

        foreach ($config['connections'] ?? [] as $name => $connection) {
            /** @var EntityManagerInterface $entityManager */
            $entityManager = $this->infrastructureServicePluginManager->build(
                EntityManagerInterface::class,
                [
                    'connection' => $connection,
                    'dbal' => $config['dbal'],
                    'orm' => $config['orm'],
                    'secondLevelCache' => $config['secondLevelCache'],
                    'logging' => $config['logging'],
                ]
            );

            /** @var DbConnection $dbConnection */
            $dbConnection = $dbConnectionService->prepareDomainObject();
            $status = $entityManager->getConnection()->ping();

            if (!$status) {
                $httpStatus = HttpHelper::STATUS_CODE_INTERNAL_SERVER_ERROR;
            }

            $dbConnection->setName($name);
            $dbConnection->setStatus(
                $status ? DbConnection::STATUS_OK : DbConnection::STATUS_DOWN
            );
            $dbConnectionArray[] = $dbConnection;
        }

        return $this->prepareResponse($dbConnectionArray, [], $httpStatus);
    }
}
