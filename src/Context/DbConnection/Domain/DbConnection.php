<?php

declare(strict_types=1);

namespace Smtm\HealthCheck\Context\DbConnection\Domain;

use Smtm\Base\Domain\ValueObjectInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DbConnection implements ValueObjectInterface
{

    public const STATUS_OK = 'ok';
    public const STATUS_DOWN = 'down';

    protected string $name;
    protected string $status;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): static
    {
        $this->status = $status;

        return $this;
    }
}
